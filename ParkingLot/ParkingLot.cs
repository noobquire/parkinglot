﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Timer = System.Timers.Timer;

namespace ParkingLot
{
    public sealed class ParkingLot
    {
        private static readonly object SLock = new object();
        private static ParkingLot _instance;
        public double Balance { get; set; }
        public event EventHandler PaymentRequest;
        public readonly Timer PaymentTimer;

        public IEnumerable<Transaction> LastTransactions => TransactionLogger.GetLastTransactions(TimeSpan.FromMinutes(1));
        public IEnumerable<Transaction> AllTransactions => TransactionLogger.GetAllTransactions();

        public double EarnedLastMinute => LastTransactions.Sum(t => t.Amount);

        private ParkingLot()
        {
            
            PaymentTimer = new Timer(ParkingLotSettings.ChargePeriod * 1000);
            PaymentTimer.Elapsed += (s, e) => OnPaymentRequest();
            PaymentTimer.Start();

            Vehicles = new List<Vehicle>();
        }

        public void RemoveVehicle(Vehicle vehicle)
        {
            if (Vehicles.Contains(vehicle))
            {
                Vehicles.Remove(vehicle);

            }

        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (Vehicles.Count + 1 < ParkingLotSettings.MaxVehicles)
            {
                double fee = 0;
                switch (vehicle.Type)
                {
                    case VehicleType.Car:
                        fee = ParkingLotSettings.CarFee;
                        break;
                    case VehicleType.Bus:
                        fee = ParkingLotSettings.BusFee;
                        break;
                    case VehicleType.Truck:
                        fee = ParkingLotSettings.TruckFee;
                        break;
                    case VehicleType.Motorcycle:
                        fee = ParkingLotSettings.MotorcycleFee;
                        break;
                }

                PaymentRequest += (s, e) => TransactionLogger.Log(vehicle.Charge(fee));
                Vehicles.Add(vehicle);
            }
            else
            {
                throw new ArgumentException("Parking lot is full");
            }
        }

        public readonly List<Vehicle> Vehicles;

        public static ParkingLot Instance
        {
            get
            {
                if (_instance != null) return _instance;
                Monitor.Enter(SLock);
                ParkingLot temp = new ParkingLot();
                Interlocked.Exchange(ref _instance, temp);
                Monitor.Exit(SLock);
                return _instance;
            }
        }
        private void OnPaymentRequest()
        {
            PaymentRequest?.Invoke(this, EventArgs.Empty);
        }
    }
}
