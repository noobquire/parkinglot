﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ParkingLot
{
    public static class Menus
    {
        private static string GetVariable(string variable)
        {
            Console.Clear();
            Console.WriteLine($"Please, enter {variable}");
            return Console.ReadLine();
        }

        private static double GetDouble(string name)
        {
            Console.Clear();
            while (true)
            {
                Console.WriteLine($"Please, enter {name}");
                try
                {
                    return double.Parse(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Please, enter a number");
                }
            }

        }

        public static Menu MainMenu => new Menu()
        {
            Title = "Manage parking lot",
            EscapeAction = () => Environment.Exit(0),
            Options = new List<MenuOption>
            {
                new MenuOption("Current balance", () => Balance.Print()),
                new MenuOption("Earnings for last 1 minute", () => LastEarnings.Print()),
                new MenuOption("Free spaces count", () => FreePlaces.Print()),
                new MenuOption("Last minute transactions", () => LastTransactions.Print()),
                new MenuOption("All transactions", () => AllTransactions.Print()),
                new MenuOption("Parked vehicles list", () => ParkedVehicles.Print()),
                new MenuOption("Add vehicle", () => AddVehicle().Print()),
                new MenuOption("Remove vehicle", () => RemoveVehicle().Print()),
                new MenuOption("Recharge balance", () => RechargeBalance().Print()),
            }
        };

        public static Menu Balance => new Menu()
        {
            Title = $"Current balance: {ParkingLot.Instance.Balance}",
        };

        public static Menu LastEarnings => new Menu()
        {
            Title = $"Earned for last 1 minute: {ParkingLot.Instance.EarnedLastMinute}",
        };

        public static Menu FreePlaces => new Menu()
        {
            Title = $"Free places on parking lot: {ParkingLotSettings.MaxVehicles - ParkingLot.Instance.Vehicles.Count}",
        };

        public static Menu LastTransactions => new Menu()
        {

            Title =
                ParkingLot.Instance.LastTransactions.Any() ?
                    $"Transactions for last minute:\n\r{string.Join("\n\r", ParkingLot.Instance.LastTransactions.Select(t => t.ToString()))}"
                    : "No transactions for given period",
        };

        public static Menu AllTransactions => new Menu()
        {
            Title = ParkingLot.Instance.AllTransactions.Any()
                ? $"All transactions:\n\r{string.Join("\n\r", ParkingLot.Instance.AllTransactions.Select(t => t.ToString()))}"
                : "There were no transactions",
        };

        public static Menu ParkedVehicles => new Menu()
        {
            Title = ParkingLot.Instance.Vehicles.Any()
                ? $"Vehicles parked on the parking lot:\n\r{string.Join("\n\r", ParkingLot.Instance.Vehicles.Select(t => $"{t} Balance: {t.Balance}"))}"
                : "The parking lot is empty",

        };

        public static Menu AddVehicle()
        {
            return new Menu()
            {
                Title = "Choose new vehicle type:",
                Options = new List<MenuOption>
                {
                    new MenuOption("Car", () => EnterVehicleName(VehicleType.Car)),
                    new MenuOption("Bus", () => EnterVehicleName(VehicleType.Bus)),
                    new MenuOption("Truck", () => EnterVehicleName(VehicleType.Truck)),
                    new MenuOption("Motorcycle", () => EnterVehicleName(VehicleType.Motorcycle)),
                }
            };
        }

        public static void EnterVehicleName(VehicleType vehicleType)
        {
            string vehicleName = GetVariable("vehicle name");
            double balance = GetDouble("initial balance");
            var vehicle = new Vehicle(vehicleName, vehicleType);
            vehicle.RechargeBalance(balance);
            ParkingLot.Instance.AddVehicle(vehicle);
            VehicleCreated(vehicle).Print();

        }

        public static Menu VehicleCreated(Vehicle vehicle) => new Menu()
        {
            Title = $"Vehicle successfully added to parking lot: {vehicle}",
            Options = new List<MenuOption>
            {
                new MenuOption("Back to main menu", () => MainMenu.Print()),
                new MenuOption("Add another vehicle", () => AddVehicle().Print()),
                new MenuOption("Vehicle list", () => ParkedVehicles.Print())
            }
        };

        public static Menu RemoveVehicle()
        {
            var menu = new Menu()
            {
                Title = "Choose vehicle to remove from parking lot",
            };
            if (!ParkingLot.Instance.Vehicles.Any()) return menu;
            foreach (var vehicle in ParkingLot.Instance.Vehicles)
            {
                menu.Options.Add(new MenuOption(vehicle.ToString(), () =>
                {
                    ParkingLot.Instance.RemoveVehicle(vehicle);
                    RemovalSuccess(vehicle).Print();
                }));
            }

            return menu;
        }
        public static Menu RemovalSuccess(Vehicle vehicle) => new Menu()
        {
            Title = $"Vehicle successfully removed from parking lot: {vehicle}",
            Options = new List<MenuOption>()
            {
                new MenuOption("Back to main menu", () => MainMenu.Print()),
                new MenuOption("Remove another vehicle", () => RemoveVehicle().Print()),
                new MenuOption("Vehicle list", () => ParkedVehicles.Print()),
            }
        };

        public static Menu RechargeBalance()
        {
            var menu = new Menu()
            {
                Title = "Choose vehicle to recharge balance",
            };
            if (!ParkingLot.Instance.Vehicles.Any()) return menu;
            foreach (var vehicle in ParkingLot.Instance.Vehicles)
            {
                menu.Options.Add(new MenuOption(vehicle.ToString(), () => EnterRechargeAmount(vehicle)));
            }
            return menu;
        }

        public static void EnterRechargeAmount(Vehicle vehicle)
        {
            double amount = GetDouble("recharge amount");
            vehicle.RechargeBalance(amount);
            RechargeSuccess(vehicle).Print();
        }

        public static Menu RechargeSuccess(Vehicle vehicle) => new Menu()
        {
            Title = $"Successsfully recharged balance of vehicle {vehicle}\n\rNew balance: {vehicle.Balance}",
            Options = new List<MenuOption>()
            {
                new MenuOption("Back to main menu", () => MainMenu.Print()),
                new MenuOption("Recharge balance of another vehicle", () => RechargeBalance().Print())
            }
        };
    }
}
