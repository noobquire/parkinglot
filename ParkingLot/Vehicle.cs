﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingLot
{
    public class Vehicle
    {
        public double Balance { get; private set; }
        public string Name { get; set; }
        public VehicleType Type;

        public Vehicle(string name, VehicleType type)
        {
            Name = name;
            Type = type;
            Balance = 0;
        }

        public void RechargeBalance(double amount)
        {
            if (amount > 0)
            {
                Balance += amount;
            }
        }

        public override string ToString()
        {
            return $"{Type} {Name}";
        }

        public Transaction Charge(double amount)
        {
            
            if (Balance < amount)
            {
                Balance -= amount * ParkingLotSettings.OverdueFineCoefficient;
                return null;
            }

            Balance -= amount;
            return new Transaction(this, DateTime.Now, amount);
        }

    }

    public enum VehicleType
    {
        Car,
        Bus,
        Truck,
        Motorcycle
    }
}
