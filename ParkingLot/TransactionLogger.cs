﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ParkingLot
{
    public static class TransactionLogger
    {
        private static string logPath = @"..\Transactions.log";
        public static void Log(Transaction transaction)
        {
            ParkingLot.Instance.PaymentTimer.Stop();
            using (StreamWriter sw = new StreamWriter(logPath, true))
            {
                sw.WriteLine(transaction);
            }
            ParkingLot.Instance.PaymentTimer.Start();
        }

        public static IEnumerable<Transaction> GetAllTransactions()
        {
            if (!File.Exists(logPath)) yield break;
            using (StreamReader sr = new StreamReader(logPath))
            {
                string line = String.Empty;
                while ((line = sr.ReadLine()) != null)
                {
                    var transaction = Transaction.Parse(line);
                    if (transaction != null) yield return transaction;
                }
                
            }
        }

        public static IEnumerable<Transaction> GetLastTransactions(TimeSpan timeFromNow)
        {
            foreach (var transaction in GetAllTransactions())
            {
               if(DateTime.Now - transaction.TimePayed < timeFromNow)
               {
                   yield return transaction;
               }
            }
        }

    }
}
