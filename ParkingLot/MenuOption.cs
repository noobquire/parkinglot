﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingLot
{
    public class MenuOption
    {
        public string Text;
        public Action Choose;
        public bool WithNumber;

        public MenuOption(string text, Action choose, bool withNumber = true)
        {
            Text = text;
            Choose = choose;
            WithNumber = withNumber;
        }
    }
}
