﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ParkingLot
{
    public class Menu
    {
        public virtual string Title { get; set; }
        public List<MenuOption> Options = new List<MenuOption>();
        private int selectedIndex = 0;
        public Action EscapeAction = () => Menus.MainMenu.Print();
        public string Hint = "Up/down arrow keys: navigate menu\n\rEnter: choose option\n\rEscape: go back/exit";

        public Menu()
        {
            Console.Clear();
        }

        public void Print()
        {
            Console.CursorVisible = false;
            Console.SetCursorPosition(0, 0);
            Console.WriteLine(Title);

            Console.WriteLine();
            for (int i = 0; i < Options.Count; i++)
            {
                var selector = selectedIndex == i ? '>' : ' ';
                var number = Options[i].WithNumber ? $"{i + 1}" : "  ";
                Console.WriteLine($"{selector} {number}. {Options[i].Text}");
            }

            Console.WriteLine();
            Console.WriteLine(Hint);
            UserInput();
        }

        private void UserInput()
        {
            var key = Console.ReadKey(true).Key;
            switch (key)
            {
                case ConsoleKey.UpArrow:
                    selectedIndex--;
                    if (selectedIndex < 0) selectedIndex = 0;
                    Print();
                    break;
                case ConsoleKey.DownArrow:
                    if (Options.Any())
                    {
                        selectedIndex = (selectedIndex + 1) % Options.Count;
                    }
                    Print();
                    break;
                case ConsoleKey.Enter:
                    if (Options.Any())
                    {
                        Options[selectedIndex].Choose();
                    }
                    else
                    {
                        EscapeAction();
                    }
                    break;
                case ConsoleKey.Escape:
                    EscapeAction();
                    break;
                default:
                    Print();
                    break;
            }
        }

       
    }
}
