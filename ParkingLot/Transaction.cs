﻿using System;
using System.Linq;

namespace ParkingLot
{
    public sealed class Transaction
    {
        public Vehicle Payer { get; set; }
        public DateTime TimePayed { get; set; }
        public double Amount { get; set; }

        public Transaction(Vehicle payer, DateTime time, double amount)
        {
            Payer = payer;
            TimePayed = time;
            Amount = amount;
        }

        public static Transaction Parse(string transaction)
        {
            // Example:
            // transaction 3.5 at 19.05.2019 13:58:00 from Car Honda R2 
            if (transaction.Split(' ').Length < 8) return null;
            string timePayedText = transaction.Split(' ')[3] + " " + transaction.Split(' ')[4];
            DateTime timePayed = DateTime.ParseExact(timePayedText, "dd.MM.yyyy HH:mm:ss", null);

            VehicleType vehicleType = Enum.Parse<VehicleType>(transaction.Split(' ')[6]);
            string vehicleName = string.Join(' ',transaction.Split(' ').Skip(7));

            double amount = double.Parse(transaction.Split(' ')[1]);

            return new Transaction(new Vehicle(vehicleName, vehicleType), timePayed, amount);
        }

        public override string ToString()
        {
            return $"Transaction {Amount} at {TimePayed:dd.MM.yyyy HH:mm:ss} from {Payer}";
        }

        
    }
}
